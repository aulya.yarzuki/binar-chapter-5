# binar-chapter-5

Project membuat halaman HTML Statis ke Server menggunakan Express dan membuat data user static untuk login di bagian backend.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:fe8738376241e24d51064c383a7f0cf5?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:fe8738376241e24d51064c383a7f0cf5?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:fe8738376241e24d51064c383a7f0cf5?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/aulya.yarzuki/binar-chapter-5.git
git branch -M main
git push -uf origin main
```

## Tech
HTML
Javascript
Node JS
Express JS
EJS

## Routes
### GET method
'/' => homepage<br>
'/game' => game page<br>
'/users' => users data (static)<br>
'/user?id=x' => user data (static) [x=1,2,3] <br>
'/login' => login page

### POST Method
'/login' => input "username" and "password"

<hr>

## Set Up

### Get the project
1. Make an empty directory for the project
2. In the empty directory, run this command to pull the project :
```
git init
git remote add origin https://gitlab.com/aulya.yarzuki/binar-chapter-5.git
git pull origin main
```
  Input your github username and password if needed
3. Install npm inside the project :
```
npm install
```

### Run the program
4. Run artisan command :
```
npm run start
```
5. Open browser, link = [http://localhost:8000](http://localhost:8000)

<hr>

### To stop the program
6. Press Ctrl+C
7. Press "y", hit Enter
